const express = require('express');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser');

// เปิดใช้งาน body parser middleware
app.use(bodyParser.json());

// เพิ่มเส้นทาง (route) สำหรับ API เพื่อดึงข้อมูลผู้ใช้จากไฟล์ JSON
app.get('/api/users/:name', (req, res) => {
    const name = req.params.name;
    // อ่านไฟล์ JSON
    fs.readFile('people.json', 'utf8', (err, data) => {
        if (err) {
            console.error('เกิดข้อผิดพลาดในการอ่านไฟล์ JSON:', err);
            return res.status(500).json({ error: 'เกิดข้อผิดพลาดในการดึงข้อมูล' });
        }

        try {
            // แปลงข้อมูล JSON เป็นอ็อบเจ็กต์ JavaScript
            const peopleData = JSON.parse(data);
            // ค้นหาข้อมูลของผู้ใช้โดยใช้ชื่อ
            const user = peopleData.find(user => user.name === name);
            if (!user) {
                return res.status(404).json({ error: 'ไม่พบข้อมูลผู้ใช้' });
            }
            // ส่งข้อมูลผู้ใช้กลับไปในรูปแบบ JSON
            res.json({ user });
        } catch (parseError) {
            console.error('เกิดข้อผิดพลาดในการแปลงข้อมูล JSON:', parseError);
            res.status(500).json({ error: 'เกิดข้อผิดพลาดในการดึงข้อมูล' });
        }
    });
});

// เพิ่มเส้นทาง (route) สำหรับ API เพื่อเพิ่มข้อมูลผู้ใช้ลงในไฟล์ JSON
app.post('/api/users/add', (req, res) => {
    const { id, name } = req.body;

    // อ่านไฟล์ JSON
    fs.readFile('people.json', 'utf8', (err, data) => {
        if (err) {
            console.error('เกิดข้อผิดพลาดในการอ่านไฟล์ JSON:', err);
            return res.status(500).json({ error: 'เกิดข้อผิดพลาดในการดึงข้อมูล' });
        }

        try {
            // แปลงข้อมูล JSON เป็นอ็อบเจ็กต์ JavaScript
            const peopleData = JSON.parse(data);

            // เพิ่มข้อมูลผู้ใช้ใหม่ลงในอาร์เรย์
            peopleData.push({ id, name });

            // บันทึกข้อมูลลงในไฟล์ JSON
            fs.writeFile('people.json', JSON.stringify(peopleData), 'utf8', err => {
                if (err) {
                    console.error('เกิดข้อผิดพลาดในการบันทึกไฟล์ JSON:', err);
                    return res.status(500).json({ error: 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' });
                }
                res.json({ message: 'เพิ่มข้อมูลผู้ใช้เรียบร้อยแล้ว' });
            });
        } catch (parseError) {
            console.error('เกิดข้อผิดพลาดในการแปลงข้อมูล JSON:', parseError);
            res.status(500).json({ error: 'เกิดข้อผิดพลาดในการดึงข้อมูล' });
        }
    });
});

// กำหนดพอร์ตและเริ่มเซิร์ฟเวอร์
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`เซิร์ฟเวอร์ API ทำงานอยู่ที่พอร์ต ${port}`);
});
